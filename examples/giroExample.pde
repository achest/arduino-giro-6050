
#include <Wire.h>
#include <giro6050.h>

Giro6050  accel(0x68);

void setup() {
//  Serial.begin(9600);

  Serial.begin(115200);
  Serial.print("\nStart Sensor Read Sensor ");
  accel.init();
  delay(100); // Wait for sensor to stabilize
}

void loop ()
{
   accel.readSensor(); 
   
   int16_t gyroX, gyroY, gyroZ;
   int16_t gyroX_filtered, gyroY_filtered, gyroZ_filtered;
   gyroX =  accel.getGyroX() ; 
   gyroY =  accel.getGyroY() ; 
   gyroZ =  accel.getGyroZ() ;
   
   gyroX_filtered = accel.getGyroXFiltered();
   gyroY_filtered = accel.getGyroYFiltered();
   gyroZ_filtered = accel.getGyroZFiltered();
   Serial.print("\nSensor Values: ");
   Serial.print(" gyroX  ");Serial.print(gyroX); 
   Serial.print(" gyroY  ");Serial.print(gyroY); 
   Serial.print(" gyroZ  ");Serial.print(gyroZ); 


   Serial.print("\nSensor Filtered Values: ");

   Serial.print(" gyroX_filtered  ");Serial.print(gyroX_filtered); 
   Serial.print(" gyroY_filtered  ");Serial.print(gyroY_filtered); 
   Serial.print(" gyroZ_filtered  ");Serial.print(gyroZ_filtered); 

}
