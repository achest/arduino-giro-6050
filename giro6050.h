#ifndef GIRO6050_H
#define GIRO6050_H


#if ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

#include <Wire.h>
//#include "libraries/Wire/Wire.h"

class Giro6050 {
   public:
   // _alfa=0.989
    Giro6050(uint8_t _IMUAddress, int16_t _zeroX =0,int16_t _zeroY=0, int16_t _zeroZ=0,double _alfa=0.7);
   ~Giro6050();
   private: 
   int16_t accX, accY, accZ;
   int16_t gyroX, gyroY, gyroZ;
   int16_t gyroX_last, gyroY_last, gyroZ_last; 
   int16_t zeroX,zeroY,zeroZ;
   double alfa ;

   double accXangle, accYangle; // Angle calculate using the accelerometer
   double temp; // Temperature
   int16_t tempRaw;
   double gyroXangle, gyroYangle; // Angle calculate using the gyro
   double compAngleX, compAngleY; // Calculate the angle using a complementary filter
   double kalAngleX, kalAngleY; // Calculate the angle using a Kalman filter
   uint8_t i2cData[14]; // Buffer for I2C data

   uint8_t iMUAddress ; // AD0 is logic low on the PCB
   uint16_t i2C_TIMEOUT ; // Used to check for errors in I2C communication

   int move;
   int sil; 
   int debug;

   public: 
   void init();
   void readSensor();
   int16_t getGyroX();
   int16_t getGyroY();
   int16_t getGyroZ();
   int16_t getGyroXFiltered();
   int16_t getGyroYFiltered();
   int16_t getGyroZFiltered();
   long getAccel();

   int movement();
   int silence ();
   void startDebug();
   void stopDebug();
   private:
   uint8_t i2cWrite(uint8_t registerAddress, uint8_t data, bool sendStop) ;
   uint8_t i2cWrite(uint8_t registerAddress, uint8_t *data, uint8_t length, bool sendStop);
   uint8_t i2cRead(uint8_t registerAddress, uint8_t *data, uint8_t nbytes);
   

};

#endif

