
#include <giro6050.h>

Giro6050::Giro6050(uint8_t _iMUAddress, int16_t _zeroX ,int16_t _zeroY, int16_t _zeroZ, double _alfa ) {

  iMUAddress = _iMUAddress;
  i2C_TIMEOUT = 1000; // Used to check for errors in I2C communication
  zeroX= _zeroX;
  zeroY= _zeroY;
  zeroZ= _zeroZ;
  alfa = _alfa;
  move = 0;
  sil = 0;
  debug = 0;
}

void Giro6050::init() {
  Wire.begin();
  i2cData[0] = 7; // Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
  i2cData[1] = 0x00; // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
  i2cData[2] = 0x00; // Set Gyro Full Scale Range to ±250deg/s
  i2cData[3] = 0x00; // Set Accelerometer Full Scale Range to ±2g
  while(i2cWrite(0x19,i2cData,4,false)); // Write to all four registers at once
  while(i2cWrite(0x6B,0x01,true)); // PLL with X axis gyroscope reference and disable sleep mode
  
  while(i2cRead(0x75,i2cData,1));
  if(i2cData[0] != iMUAddress) { // Read "WHO_AM_I" register
    Serial.print(F("Error reading sensor"));
    while(1);
  }
  
}
Giro6050::~Giro6050() {

}

uint8_t Giro6050::i2cWrite(uint8_t registerAddress, uint8_t data, bool sendStop) {
  return i2cWrite(registerAddress,&data,1,sendStop); // Returns 0 on success
}

uint8_t Giro6050::i2cWrite(uint8_t registerAddress, uint8_t *data, uint8_t length, bool sendStop) {
  Wire.beginTransmission(iMUAddress);
  Wire.write(registerAddress);
  Wire.write(data, length);
  uint8_t rcode = Wire.endTransmission(); // Returns 0 on success
  if (rcode) {
    Serial.print(F("i2cWrite failed: "));
    Serial.println(rcode);
  }
  return rcode; // See: http://arduino.cc/en/Reference/WireEndTransmission
}

uint8_t Giro6050::i2cRead(uint8_t registerAddress, uint8_t *data, uint8_t nbytes) {
  uint32_t timeOutTimer;
  Wire.beginTransmission(iMUAddress);
  Wire.write(registerAddress);
  uint8_t rcode = Wire.endTransmission(); // Don't release the bus
  if (rcode) {
    Serial.print(F("i2cRead failed: "));
    Serial.println(rcode);
    return rcode; // See: http://arduino.cc/en/Reference/WireEndTransmission
  }
  Wire.requestFrom(iMUAddress, nbytes); // Send a repeated start and then release the bus after reading
  for (uint8_t i = 0; i < nbytes; i++) {
    if (Wire.available())
      data[i] = Wire.read();
    else {
      timeOutTimer = micros();
      while (((micros() - timeOutTimer) < i2C_TIMEOUT) && !Wire.available());
      if (Wire.available())
        data[i] = Wire.read();
      else {
        Serial.println(F("i2cRead timeout"));
        return 5; // This error value is not already taken by endTransmission
      }
    }
  }
  return 0; // Success
}
void Giro6050::readSensor() {
  while(i2cRead(0x3B,i2cData,14));
  accX = ((i2cData[0] << 8) | i2cData[1]);
  accY = ((i2cData[2] << 8) | i2cData[3]);
  accZ = ((i2cData[4] << 8) | i2cData[5]);
  tempRaw = ((i2cData[6] << 8) | i2cData[7]);
  gyroX = ((i2cData[8] << 8) | i2cData[9]);
  gyroY = ((i2cData[10] << 8) | i2cData[11]);
  gyroZ = ((i2cData[12] << 8) | i2cData[13]);
    
  gyroX +=zeroX;
  gyroY +=zeroY;
  gyroZ +=zeroZ;
  
  gyroX_last  = gyroX_last*alfa+(1-alfa)*gyroX; 
  gyroY_last = gyroY_last*alfa+(1-alfa)*gyroY;
  gyroZ_last = gyroZ_last*alfa+(1-alfa)*gyroZ;  
}


int16_t Giro6050::getGyroX(){
   return gyroX ;
}
int16_t Giro6050::getGyroY(){
   return gyroY ;
}
int16_t Giro6050::getGyroZ(){
   return gyroZ ;
}
 int16_t Giro6050::getGyroXFiltered(){
   return gyroX_last ;
}
 int16_t Giro6050::getGyroYFiltered(){
    return gyroY_last;
}
 int16_t Giro6050::getGyroZFiltered(){
   return gyroZ_last ;
}

long Giro6050::getAccel(){
    long x = gyroX_last/20;
    long y = gyroY_last/20;
    long z = gyroZ_last/20;

    long acc = x*x+ y*y+z*z;
//    int16_t acc = gyroX_last*gyroX_last+gyroY_last*gyroY_last+gyroZ_last*gyroZ_last;
  if (debug == 1) {
   Serial.print("Acce: ");
   Serial.print(acc);    
   Serial.print(" X: ");
   Serial.print(x);  
   Serial.print(" X2: ");
   Serial.print(x*x);    
   Serial.print(" Y2: ");
   Serial.print(y*y);    
  Serial.print(" Z2: ");
   Serial.print(z*z);    


  }
    return acc; 
}

int Giro6050::movement()
{
   readSensor();
   long  acc = getAccel();
   if (acc > 2010) {
    move ++;
   } else {
     move = 0;    	    
   }
    
   return move;
}
int Giro6050::silence()
{
   readSensor();
   long acc = getAccel();
   if (acc < 300) {
    sil ++;
   } else {
    sil = 0;		    
   }
    
   return sil;
}

void Giro6050::startDebug(){
   debug = 1;
}
  void Giro6050::stopDebug() {
  debug  = 0;
}
   


